print('Would you like to encrypt or decrypt a cipher?\n1 - Encrypt\n2 - Decrypt\n3 - exit')
while True:
    try:
        choice = int(input())
        break
    except ValueError:
        print("Please, enter '1' for encryption or '2' for decryption.")
loop = True
while loop:
    if choice == 1:
        print('Please, enter a text that should be encrypted:')
        text = input()
        if not text or text == "NULL":
                print("The final cipher is", text)
        else:
            print('Please, enter a number of repetitions:')
            while True:
                try:
                    n = int(input())
                    break
                except ValueError:
                    print("Please, enter only whole numbers.")
            if n <= 0:
                print("The final cipher is", text)
            else:
                def encrypt (text, n):
                    if not text or text == "NULL":
                        return text
                    else:
                        if n <= 0:
                            return text
                        else:
                            new_text = text
                            while n > 0:
                                n -= 1
                                a, b = new_text[1::2], new_text[0::2]
                                new_text = a + b
                            print('new_cipher =', new_text)
                            print("The final cipher is", new_text.lower())
                            return new_text
                encrypt(text, n)
        print('\nWould you like to decrypt a cipher or encrypt another one?\n1 - Encrypt\n2 - Decrypt\n3 - exit')
        while True:
            try:
                choice = int(input())
                break
            except ValueError:
                print("Please, enter only whole numbers.")

    elif choice == 2:
        print("\nPlease, enter a cipher that should be decrypted (you may copy text from the 'new_cipher' if available):")
        encrypted_text = input()
        if not encrypted_text or encrypted_text == "NULL":
            print("The decrypted cipher is", encrypted_text)
        else:
            print('Please, enter a number of repetitions:')
            while True:
                try:
                    n = int(input())
                    break
                except ValueError:
                    print("Please, enter only whole numbers.")
            if n <= 0:
                print("The final cipher is", encrypted_text)
            else:
                def decrypt(encrypted_text, n):
                    if not encrypted_text or encrypted_text == "NULL":
                        return encrypted_text
                    else:
                        if n <= 0:
                            return encrypted_text
                        else:
                            new_d_text = encrypted_text
                            l = len(encrypted_text)
                            while n > 0:
                                n -= 1
                                if l%2 == 0:
                                    a = new_d_text[:l//2]
                                    b = new_d_text[l//2:]
                                    c = []
                                    for i in range(0, l//2):
                                        c.append(b[i])
                                        c.append(a[i])
                                else:
                                    a = new_d_text[:l//2]
                                    b = new_d_text[l//2:-1]
                                    c = []
                                    for i in range(0, l//2):
                                        c.append(b[i])
                                        c.append(a[i])
                                    c.append(new_d_text[-1])
                                new_d_text = c
                            print("The decrypted cipher is", "".join(c))
                            return c
                choice = 3
                decrypt(encrypted_text, n)
        print('\nWould you like to encrypt a cipher or decrypt another one?\n1 - Encrypt\n2 - Decrypt\n3 - exit')
        while True:
            try:
                choice = int(input())
                break
            except ValueError:
                print("Please, enter only whole numbers.")
    elif choice == 3:
        break
    else:
        print("Oops! That was no valid number. Please, try again.")
        while True:
            try:
                choice = int(input())
                break
            except ValueError:
                print("Please, enter '1' for encryption, '2' for decryption or '3' to exit the program.")
print("Keep your data safe")