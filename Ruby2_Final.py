from collections import Counter

print("Please, enter the text file path.")
path = str(input())

while True:
    try:
        if path == "1":
            break
        def prepositions_conjunctions_articles(path):
            with open(path, "r", encoding='utf-8') as reader:
                words = reader.read()
            s = Counter(words.lower().split()).most_common(3)
            if len(s) >= 3:
                print(s)
                return s
            else:
                print("\nThere are less than three unique words in the text.")
                return []
        prepositions_conjunctions_articles(path)
        break
    except:
        print("Please, enter the correct one or press '1' to exit.")
        path = str(input())